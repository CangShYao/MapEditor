// #pragma once
#ifndef PAINT_H
#define PAINT_H
#include "MyDataType.h"
#include "Calculate.h"
#include "afxwin.h"
void DrawPnt(CClientDC * dc, PNT_STRU point);
void ShowAllPnt(CClientDC * dc, CFile * pntTmpF, int pntNum);
void ShowAllPnt(CClientDC * dc, CFile * pntTmpF, int pntNum, double zoomOffset_x, double zoomOffset_y, double zoom, char isDel);

// 构造线段的函数声明
void DrawSeg(CClientDC * dc, LIN_NDX_STRU line, POINT point1, POINT point2);
void ShowAllLin(CClientDC * dc, CFile * LinTmpNdxF, CFile * LinTmpDatF, int LinNum);
void ShowAllLin(CClientDC * dc, CFile * LinTmpNdxF, CFile * LinTmpDatF, int LinNum, double zoomOffset_x, double zoomOffset_y, double zoom, char isDel);

void DrawReg(CClientDC * dc, REG_NDX_STRU Region, POINT * pt, long nPnt);
void ShowAllReg(CClientDC * dc, CFile * RegTmpNdxF, CFile * RegTmpDatF, int RegNum, double zoomOffset_x, double zoomOffset_y, double zoom, char isDel);
#endif
