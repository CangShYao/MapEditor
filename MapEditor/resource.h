//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MapEditor.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_POPUP_EDIT                  119
#define ID_STATUSBAR_PANE1              120
#define ID_STATUSBAR_PANE2              121
#define IDS_STATUS_PANE1                122
#define IDS_STATUS_PANE2                123
#define IDS_TOOLBAR_STANDARD            124
#define IDS_TOOLBAR_CUSTOMIZE           125
#define ID_VIEW_CUSTOMIZE               126
#define IDR_MAINFRAME                   128
#define IDR_MAINFRAME_256               129
#define IDR_MapEditorTYPE               130
#define IDR_THEME_MENU                  200
#define ID_SET_STYLE                    201
#define ID_VIEW_APPLOOK_WIN_2000        205
#define ID_VIEW_APPLOOK_OFF_XP          206
#define ID_VIEW_APPLOOK_WIN_XP          207
#define ID_VIEW_APPLOOK_OFF_2003        208
#define ID_VIEW_APPLOOK_VS_2005         209
#define ID_VIEW_APPLOOK_VS_2008         210
#define ID_VIEW_APPLOOK_OFF_2007_BLUE   215
#define ID_VIEW_APPLOOK_OFF_2007_BLACK  216
#define ID_VIEW_APPLOOK_OFF_2007_SILVER 217
#define ID_VIEW_APPLOOK_OFF_2007_AQUA   218
#define ID_VIEW_APPLOOK_WINDOWS_7       219
#define IDS_EDIT_MENU                   306
#define IDD_CREATE_FILE                 314
#define IDC_CREATE_FILE_ADDRESS         1000
#define IDC_BUTTON1                     1001
#define IDC_CREATE_FILE_CHANGE_ADDRESS_BTN 1001
#define IDC_CREATE_FILE_STATIC          1002
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_WINDOW_ZOOM_IN               32778
#define ID_WINDOW_ZOOM_OUT              32779
#define ID_WINDOW_MOVE                  32780
#define ID_WINDOW_RESET                 32781
#define ID_WINDOW_SHOW_POINT            32782
#define ID_WINDOW_SHOW_LINE             32783
#define ID_WINDOW_SHOW_REGION           32784
#define ID_Menu                         32785
#define ID_32786                        32786
#define ID_32787                        32787
#define ID_32788                        32788
#define ID_32789                        32789
#define ID_Menu32790                    32790
#define ID_32791                        32791
#define ID_32792                        32792
#define ID_32793                        32793
#define ID_32794                        32794
#define ID_32795                        32795
#define ID_32796                        32796
#define ID_32797                        32797
#define ID_32798                        32798
#define ID_32799                        32799
#define ID_32800                        32800
#define ID_32801                        32801
#define ID_32802                        32802
#define ID_32803                        32803
#define ID_32804                        32804
#define ID_32805                        32805
#define ID_32806                        32806
#define ID_32807                        32807
#define ID_32808                        32808
#define ID_32809                        32809
#define ID_32810                        32810
#define ID_32811                        32811
#define ID_32812                        32812
#define ID_Menu32813                    32813
#define ID_32814                        32814
#define ID_32815                        32815
#define ID_32816                        32816
#define ID_32817                        32817
#define ID_32818                        32818
#define ID_POINT_CREATE                 32819
#define ID_POINT_MOVE                   32820
#define ID_POINT_DELETE                 32821
#define ID_POINT_SHOW_DELETED           32822
#define ID_POINT_UNDELETE               32823
#define ID_POINT_MODIFY_PARAMETER       32824
#define ID_POINT_SET_DEFPARAMETER       32825
#define ID_LINE_CREATE                  32826
#define ID_LINE_MOVE                    32827
#define ID_LINE_DELETE                  32828
#define ID_LINE_SHOW_DELETED            32829
#define ID_LINE_UNDELETE                32830
#define ID_LINE_DELETE_DOT              32831
#define ID_LINE_ADD_DOT                 32832
#define ID_LINE_LINK                    32833
#define ID_LINE_MODIFY_PARAMETER        32834
#define ID_LINE_SET_DEFPARAMETER        32835
#define ID_REGION_CREATE                32836
#define ID_REGION_MOVE                  32837
#define ID_REGION_DELETE                32838
#define ID_REGION_SHOW_DELETED          32839
#define ID_REGION_UNDELETE              32840
#define ID_REGION_MODIFY_PARAMETER      32841
#define ID_REGION_SET_DEFPARAMETER      32842
#define ID_FILE_OPEN_POINT              32843
#define ID_FILE_OPEN_LINE               32844
#define ID_FILE_OPEN_REGION             32845
#define ID_FILE_SAVE_POINT              32846
#define ID_FILE_SAVE_LINE               32847
#define ID_FILE_SAVE_REGION             32848
#define ID_FILE_SAVE_AS_POINT           32849
#define ID_FILE_SAVE_AS_LINE            32850
#define ID_FILE_SAVE_AS_REGION          32851

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        315
#define _APS_NEXT_COMMAND_VALUE         32852
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
