#ifndef CALCULATE_H
#define CALCULATE_H
#include "MyDataType.h"
#include "WriteOrRead.h"
#include <math.h>
double Distance(double x1, double y1, double x2, double y2);
bool isSmall(double x1, double x2);
PNT_STRU FindPnt(CPoint mousePoint, int pntNum, CFile * pntTmpF, int &nPnt);

// D_DOT和POINT相互转换
void PntToDot(D_DOT * dot, POINT * pnt, int num);
void PntToDot(D_DOT &dot, POINT pnt);
void DotToPnt(POINT * pnt, D_DOT * dot, int num);
void DotToPnt(POINT &pnt, D_DOT dot);

// 计算鼠标单击位置到线之间的距离
double DisPntToSeg(D_DOT pt1, D_DOT pt2, D_DOT pt);

// 查找里鼠标最近的线的函数声明
LIN_NDX_STRU FindLin(CFile * LinTmpNdxF, CFile * LinTmpDatF, CPoint mousePoint, int LinNum, int &nLinNdx);

// 数据坐标系转换到窗口坐标系的函数声明
void PntDPtoVP(D_DOT &pt, double zoom, double offset_x, double offset_y);

void PntVPtoDP(D_DOT &pt, double zoom, double offset_x, double offset_y);

// 计算矩形中心的函数声明
D_DOT GetCenter(RECT rect);

// 计算拉框放大时放大倍数的函数声明
void modulusZoom(RECT client, RECT rect, double &zoom);

BOOL PtInPolygon(CPoint p, D_DOT * ptPolygon, int nCount);

// 查找离鼠标点最近区的函数声明
REG_NDX_STRU FindReg(CFile *  RegTmpNdxF, CFile * RegTmpDatF, CPoint mousePoint, int RegNum, int &nRegNdx);

// 改变线的点数据
void AlterLindot(CFile * LinTmpDatF, LIN_NDX_STRU startLine, LIN_NDX_STRU endLine, int start, int end, long allDatOff);
#endif